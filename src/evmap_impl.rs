use crate::{LruReadHandle, ThreadsafeLru};
use crossbeam::channel::{Receiver, Sender};
use evmap::{ReadHandle, ShallowCopy, WriteHandle};
use std::{
    collections::{hash_map, VecDeque},
    hash::Hash,
};

pub struct LruCache<K, V>
where
    K: Clone + Eq + Hash,
    V: Clone + Eq + ShallowCopy,
{
    max_items: usize,
    write_handle: WriteHandle<K, V, (), hash_map::RandomState>,
    read_handle: ReadHandle<K, V, (), hash_map::RandomState>,
    dq: VecDeque<K>,
    write_side_chan: Receiver<K>,
    read_side_chan: Sender<K>,
}

#[derive(Clone)]
pub struct CacheReadHandle<K, V>
where
    K: Clone + Eq + Hash,
    V: Clone + Eq + ShallowCopy,
{
    read_h: ReadHandle<K, V, (), hash_map::RandomState>,
    chan: Sender<K>,
}

use std::iter::FromIterator;
impl<K, V> CacheReadHandle<K, V>
where
    K: Clone + Eq + Hash,
    V: Clone + Eq + ShallowCopy,
{
    /// Read all values in the map, and transform them into a new collection.
    pub fn map_into<Map, Collector, Target>(&self, f: Map) -> Collector
    where
        Map: FnMut(&K, &[V]) -> Target,
        Collector: FromIterator<Target>,
    {
        self.read_h.map_into(f)
    }
}

impl<K, V> LruCache<K, V>
where
    K: Clone + Eq + Hash,
    V: Clone + Eq + ShallowCopy,
{
    pub fn new(max_items: usize) -> Self {
        let (read_handle, write_handle) = evmap::new();
        let (read_side_chan, write_side_chan) = crossbeam::channel::bounded(24);
        LruCache {
            max_items,
            read_handle,
            write_handle,
            dq: VecDeque::new(),
            read_side_chan,
            write_side_chan,
        }
    }
}

impl<K, V> ThreadsafeLru<K, V, CacheReadHandle<K, V>> for LruCache<K, V>
where
    K: Clone + Eq + Hash,
    V: Clone + Eq + ShallowCopy,
{
    fn insert(&mut self, key: K, value: V) {
        // First process any access updates from the read handles
        while let Ok(msg) = self.write_side_chan.try_recv() {
            let mut index = None;
            // This potentially scans the whole list and is slow. For our current purposes it should
            // be ok.
            for (i, entry) in self.dq.iter().enumerate() {
                if *entry == msg {
                    index = Some(i)
                }
            }
            // Move the item to the front of the q
            if let Some(i) = index {
                let t = self.dq.remove(i);
                if let Some(t) = t {
                    self.dq.push_front(t);
                }
            }
        }

        if self.dq.len() >= self.max_items {
            let maybe_removed = self.dq.pop_back();
            if let Some(r) = maybe_removed {
                self.write_handle.empty(r);
            }
        }

        self.dq.push_front(key.clone());
        self.write_handle.insert(key, value);
        self.write_handle.refresh();
    }

    fn read_handle(&self) -> CacheReadHandle<K, V> {
        CacheReadHandle {
            read_h: self.read_handle.clone(),
            chan: self.read_side_chan.clone(),
        }
    }
}

impl<K, V> LruReadHandle<K, V> for CacheReadHandle<K, V>
where
    K: Clone + Eq + Hash,
    V: Clone + Eq + ShallowCopy,
{
    fn get(&self, key: &K) -> Option<V> {
        self.read_h.get_and(key, |val| {
            // Tell the write side (the side with the dq) we just accessed an element
            let _ = self.chan.try_send(key.clone());
            // Item is guaranteed to exist if we enter this closure
            val[0].clone()
        })
    }

    fn len(&self) -> usize {
        self.read_h.len()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::prelude::*;
    use std::collections::HashSet;
    use std::sync::{Arc, Barrier};

    #[test]
    fn single_threaded() {
        let mut map = LruCache::new(5);
        map.insert(1, "one");
        assert_eq!(map.read_handle().len(), 1);
        map.insert(2, "two");
        map.insert(3, "three");
        map.insert(4, "four");
        map.insert(5, "five");
        assert_eq!(map.read_handle().len(), 5);
        // We max out at 5
        map.insert(6, "six");
        assert_eq!(map.read_handle().len(), 5);
        // One should've been pushed out
        assert_eq!(map.read_handle().get(&1), None);
        // Access two, bringing it to the front
        assert_eq!(map.read_handle().get(&2), Some("two"));
        // Write again
        map.insert(7, "seven");
        // Three should be pushed out
        assert_eq!(map.read_handle().get(&3), None);
    }

    #[test]
    fn multi_threaded() {
        let mut map = LruCache::new(5);
        let reader_a = map.read_handle();
        let reader_b = map.read_handle();
        let reader_c = map.read_handle();
        let barrier = Arc::new(Barrier::new(4));
        let ba = barrier.clone();
        let bb = barrier.clone();
        let bc = barrier.clone();
        // The write-side thread (the one that does inserts)
        let res = std::thread::spawn(move || {
            for i in 1..10 {
                map.insert(i, i.to_string());
            }
            ba.wait();
            map
        });
        // Two "read" sides
        std::thread::spawn(move || {
            // This one touches 2 and 8. The while is to ensure it accesses them once (the writer
            // may not have written them yet)
            while reader_a.get(&6).is_none() {}
            while reader_a.get(&8).is_none() {}
            bb.wait();
        });
        std::thread::spawn(move || {
            while reader_b.get(&7).is_none() {}
            while reader_b.get(&9).is_none() {}
            bc.wait();
        });
        barrier.wait();
        // We must join the write thread or the writer will be dropped and the map blown up. This
        // cost me a fair amount of hair.
        let m = res.join().unwrap();
        assert_eq!(reader_c.len(), 5);
        // We know 2,3,7,8 are the four most-recently-accessed elements, so ensure they're at the
        // front of the lru queue
        let most_recent_four_set: HashSet<_> = m.dq.iter().take(4).collect();
        assert!(most_recent_four_set.contains(&6));
        assert!(most_recent_four_set.contains(&7));
        assert!(most_recent_four_set.contains(&8));
        assert!(most_recent_four_set.contains(&9));
    }

    proptest! {
        #[test]
        fn single_threaded_prop(size in 1..100usize, inserts in 0..1000usize) {
            let mut map = LruCache::new(size);
            for i in 0..inserts {
                map.insert(i, i.to_string());
                if i < size {
                    assert_eq!(map.read_handle().len(), i + 1)
                } else {
                    assert_eq!(map.read_handle().len(), size)
                }
            }
        }

        #[test]
        fn multi_threaded_prop(size in 1..100usize, (inserts, indicies) in inserts_and_gets()) {
            let mut map = LruCache::new(size);
            let reader_a = map.read_handle();
            let reader_b = map.read_handle();
            let reader_c = map.read_handle();
            let barrier = Arc::new(Barrier::new(4));
            let ba = barrier.clone();
            let bb = barrier.clone();
            let bc = barrier.clone();

            let res = std::thread::spawn(move || {
                for i in 1..=inserts {
                    map.insert(i, i.to_string());
                }
                ba.wait();
                map
            });
            let indicies2 = indicies.clone();
            std::thread::spawn(move || {
                for ix in indicies {
                    reader_a.get(&ix);
                }
                bb.wait();
            });
            std::thread::spawn(move || {
                for ix in indicies2 {
                    reader_b.get(&ix);
                }
                bc.wait();
            });
            barrier.wait();
            let _m = res.join().unwrap();
            if inserts >= size {
                assert_eq!(reader_c.len(), size);
            } else {
                assert_eq!(reader_c.len(), inserts);
            }
        }

    }

    prop_compose! {
        fn inserts_and_gets()(inserts in 1..1000usize)
                             (indicies in prop::collection::vec(0..inserts, 500),
                              inserts in Just(inserts))
            -> (usize, Vec<usize>) {
           (inserts, indicies)
        }
    }
}
